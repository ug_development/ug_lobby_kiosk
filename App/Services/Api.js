import apisauce from 'apisauce'
import { Platform } from 'react-native'
import { API_URL, TENANT_NAME } from '../Config/AppConfig'
import { store } from '../Containers/App'

const create = (baseURL = API_URL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'tenant': TENANT_NAME,
      'platform': Platform.OS
    },
    timeout: 10000
  })

  const login = (username, password) => {
    return api.post('/Users/login', {'email': username, 'password': password})
  }

  const getProfile = (accessToken) => {
    api.setHeaders({
      Authorization: accessToken, 'Content-Type': 'application/json'
    })
    return api.get('/guards/getProfile')
  }

  const getAllResidents = (searchTerm) => {
    const accessToken = store.getState().auth.accessToken
    const idProperty = store.getState().auth.userProfile.idProperty
    api.setHeaders({
      Authorization: accessToken, 'Content-Type': 'application/json',
    })
    return api.get('/Properties/'+idProperty+'/ResidentsList?searchQuery='+searchTerm+'&onlyPrimaryAccounts=true')
  }

  const fileUpload = (formdata) => {
    const accessToken = store.getState().auth.accessToken
    api.setHeaders({ Authorization: accessToken, Accept: 'application/json', 'Content-Type': 'multipart/form-data' })
    return api.post('/fileUpload/', formdata)
  }

  const createInstantPass = (passTemplate) => {
    const accessToken = store.getState().auth.accessToken
    api.setHeaders({
      Authorization: accessToken, 'Content-Type': 'application/json',
    })
    return api.post('/VisitorPasses/createInstantPass', passTemplate)
  }

  const getPassRequest = (passCodeText) => {
    const accessToken = store.getState().auth.accessToken
    api.setHeaders({
      Authorization: accessToken, 'Content-Type': 'application/json',
    })
    return api.get('/VisitorPasses/getPassDetail?passCode=' + passCodeText)
  }

  const rescanRequest = (scanPass) => {
    const accessToken = store.getState().auth.accessToken
    api.setHeaders({
      Authorization: accessToken, 'Content-Type': 'application/json',   
    })
    return api.post('/VisitorPasses/createScan', scanPass)
  }

  return {
    login,
    getProfile,
    getAllResidents,
    fileUpload,
    createInstantPass,
    getPassRequest,
    rescanRequest,
  }
}

export default {
  create
}
