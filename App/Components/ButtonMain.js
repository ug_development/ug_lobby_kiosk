import React, {useState} from 'react';
import { StyleSheet, Pressable, Text} from 'react-native';
import Colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
import LinearGradient from 'react-native-linear-gradient';
import Fonts from '../Themes/Fonts';
 
export default function ButtonRound(props){
    
    return(
            <Pressable onPress={props.onPress} style={{...styles.buttonRound,...props.styles}} >
                <LinearGradient
                    colors={[props.linearGradientLeft,props.linearGradientRight]}
                    /*colors={props.linearGradient}*/
                    style={styles.linearGradient} 
                    start={{ x: 0, y: 0 }}
                >
                    <Text style={styles.btnText}>{props.name}</Text>
                </LinearGradient>
            </Pressable>
        );
}

const styles= StyleSheet.create({
    buttonRound: {
        //marginTop: 20,
        width: '100%',
        maxWidth: "100%",
        backgroundColor: Colors.transparent,
        //paddingVertical: 15,
        height: '100%',
        flexDirection: "column",
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 5
    },
    linearGradient:{
        width: '100%',
        //paddingVertical: 15,
        height: '100%',
        flexDirection: "column",
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: 5
    },
    btnText: {
        color: Colors.snow,
        fontFamily: Fonts.type.main,
        fontSize: Fonts.size.h5
    }
    
});