import React from 'react'
import { Modal, StyleSheet, View } from 'react-native'
import LottieView from 'lottie-react-native'

import Colors from '../Themes/Colors'

export default function LottieLoading (props) {
  return (
    <Modal visible={props.visible} statusBarTranslucent transparent>
        <View style={styles.container} /*onPress={props.close}*/>
            <LottieView style={{marginTop: -100}} source={require('../Images/ug_guard_loader.json')} autoPlay loop />
        </View>
    </Modal>
  )
}
//no-internet-connection-empty-state.json
//<LottieView style={{marginTop: -150}} source={require('../Images/ug_guard_loader.json')} autoPlay loop />
const styles = StyleSheet.create({

  container: {
    height: '150%', 
    width: '120%', 
    flexDirection: 'column', 
    alignItems: 'center', 
    justifyContent: 'center', 
    backgroundColor: Colors.modalBGTop,
    }
})
