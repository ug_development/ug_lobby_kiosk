import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../Themes'
const { height, width } = Dimensions.get('window')

export default StyleSheet.create({

  container: {
    width: width*1.1,
    height: height,
    backgroundColor: Colors.background,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  time: {
    position: 'absolute',
    top: height*0.12,
    right: width*0.1,
    color: Colors.ricePaper,
  },
  mainHeading: {
    color: Colors.snow,
    fontSize: height*0.065,
    marginVertical: height*0.05
  },
  subHeading: {
    color: Colors.ricePaper,
    fontSize: height*0.03,
    lineHeight: 20,
    alignSelf: 'flex-end',
  }

  
})
