import React, {useState} from 'react';
import { StyleSheet, Pressable, Text, View, Dimensions, ImageBackground} from 'react-native';
import Colors from '../Themes/Colors';
import metrics from '../Themes/Metrics';
import Fonts from '../Themes/Fonts';
const { height, width } = Dimensions.get('window')
const userImageUG = 'https://urbangate-dev.s3.ap-south-1.amazonaws.com/Dummy/image-crop-picker/dummyMale%403x.png';

export default function UserCard(props){
    const {id, name, unit, imgUrl} = props.data;
    return(
            <Pressable onPress={()=> props.navigation.navigate('UserDetail', {visitorType: props.visitorType, relation: props.relation, data: props.data})} >
                <ImageBackground source={{uri: props.data['resident.profileImageUrl'] ? props.data['resident.profileImageUrl'] : userImageUG}} style={{width: width*0.18, height: '100%', marginRight: 20}}>
                    <View style={{position: 'absolute', left: 0, bottom: 0, padding: 10, width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.50)'}}>
                        <Text numberOfLines= {1} style={{color: 'white'}}>{props.data['resident.firstName']}</Text>
                        <Text style={{color: 'white'}}>{props.data['unit.name']}</Text>
                    </View>
                </ImageBackground>
            </Pressable>
        );
}

const styles= StyleSheet.create({
    buttonRound: {
        //marginTop: 20,
        width: '100%',
        maxWidth: "100%",
        backgroundColor: Colors.transparent,
        //paddingVertical: 15,
        height: '100%',
        flexDirection: "column",
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: metrics.bordRadiusSmall
    },
    linearGradient:{
        width: '100%',
        //paddingVertical: 15,
        height: '100%',
        flexDirection: "column",
        alignItems: "center",
        justifyContent: 'center',
        borderRadius: metrics.bordRadiusSmall
    },
    btnText: {
        color: Colors.snow,
        fontFamily: Fonts.type.main,
        fontSize: Fonts.size.btnHeading
    }
    
});