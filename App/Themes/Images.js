// leave off @2x/@3x
const images = {
  logo: require('../Images/logo.png'),
  darkLogo: require('../Images/darkLogo.png'),
  guest: require('../Images/guest.png'),
  material: require('../Images/material.png'),
  delivery: require('../Images/delivery.png'),
  service: require('../Images/service.png'),
  relative: require('../Images/relative.png'),
  friend: require('../Images/friend.png'),
  cam: require('../Images/cam.png'),
  tick: require('../Images/tick.png'),
  err: require('../Images/error.png'),
  
}

export default images
