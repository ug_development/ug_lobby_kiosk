import React, { useEffect, useState } from 'react'
import { View, StatusBar, Keyboard, Modal, Text, Image,Linking } from 'react-native'
import ReduxNavigation from '../Navigation/ReduxNavigation'
import { connect } from 'react-redux'
import StartupActions from '../Redux/StartupRedux'
import ReduxPersist from '../Config/ReduxPersist'
import NetInfo from "@react-native-community/netinfo"
import LottieView from 'lottie-react-native'
import { useDispatch, useSelector } from 'react-redux'

// Styles
import styles from './Styles/RootContainerStyles'
import { Colors } from '../Themes'
import { props } from 'ramda'


const RootContainer = (props)=> {

  const [connection,setConnection]=useState("")
  useEffect(()=> {
    if (!ReduxPersist.active) {
      props.startup()
    }
  });

  useEffect(()=> {
      const unsubscribe = NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      setConnection(state.isConnected)
    });

  },[])



  return (
    <View style={styles.applicationView}>
      {
        connection ? (
          null
        ) : (
          <Modal visible={true} statusBarTranslucent transparent>
            <View style={{backgroundColor: Colors.background, width: '100%', height: '100%', alignItems: 'center', flexDirection: 'column', justifyContent: 'center'}} >      
              <Text style={{color: Colors.snow, paddingBottom: 200, lineHeight: 30, fontSize: 20, textAlign: 'center', width: '70%'}}>OOPS!!<Text></Text>{`\nSeems like you are not connected to the internet.`}</Text>
              <LottieView style={{width: "100%", height: 250, position: 'absolute', bottom: -25}} source={require('../Images/no-internet-connection-empty-state.json')} autoPlay loop />
            </View>
          </Modal>
        )
      }

      <View style={styles.applicationView}>
        <StatusBar hidden={true} backgroundColor={Colors.background}/>
        <ReduxNavigation />
      </View>
    </View>
  )
}

 
// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = (dispatch) => ({
  startup: () => dispatch(StartupActions.startup())
})

export default connect(null, mapDispatchToProps)(RootContainer)