import React, { useState, useEffect } from 'react'
import { Pressable, Text, Image, View, Dimensions, TextInput, ImageBackground, Alert } from 'react-native'
import { Images, Colors } from '../Themes'
const { height, width } = Dimensions.get('window');
import LottieView from 'lottie-react-native'
import LottieLoading from '../Components/LottieLoading'

import CreatePassActions from '../Redux/CreatePassRedux'
import DateDisplay from '../Components/DateDisplay'
import { useDispatch, useSelector } from 'react-redux'

// Styles
import styles from './Styles/UserDetail.styles'
import ButtonMain from '../Components/ButtonMain'
import ImagePicker from 'react-native-image-crop-picker'
import { Fragment } from 'react'
const userImageUG = 'https://urbangate-dev.s3.ap-south-1.amazonaws.com/Dummy/image-crop-picker/dummyMale%403x.png';

const passTemplate = {
  idGuardPost: null,
  scanType: 'ENTRY',
  idResident: null,
  idProperty: null,
  idPropertyUnit: null,
  visitorType: '',
  currentStatus: 'AWAITING_RESPONSE_ENTRY',
  entryTimeExpected: '',
  exitTimeExpected: '',
  visitor: {
      idVisitor: null,
      firstName: '',
      lastName: '',
      profileImageUrl: '',
      proofFrontSideUrl: '',
      proofBackSideUrl: '',
      recordedTemperature: null,
      isMasked: false
  },
  members: [],
  vehicles: []
}

export default function UserDetail(props){
  const {visitorType, relation, data} = props.navigation.state.params;
  const [name, setName] = useState('');
  const [mobile, setMobile] = useState('');
  const [camImage, setCamImage] = useState('');
  const [loading, setLoading] = useState(false)

  const createPass = useSelector(store => store.createPass);
  const auth = useSelector(store => store.auth);
  const dispatch = useDispatch();

  const openCamera = (type) => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: false
    }).then(image => {
      if((image.mime === "image/jpeg") || (image.mime === "image/png")){
        //setPicUploading(true)
        var formdata = new FormData()
        formdata.append('file', {
          uri: image.path,
          // Platform.OS === "android" ? image.path : image.path.replace("file://", "")
          type: image.mime,
          name: image.path.split('/').pop()
        })
        formdata.append('path', 'Guard/')
        dispatch(CreatePassActions.visitorPhotoUploadRequest(formdata))
      } else{
        Alert.alert("Warning", 'Only jpeg and png formats allowed')
      }      
    })
  }

  useEffect(()=>{
    if(createPass.photoUrlAvailable){
      setCamImage(createPass.visitorPhoto)
      dispatch(CreatePassActions.stateReset({
        photoUrlAvailable: false,
        visitorPhoto: null
      }))
    }
  },[createPass.photoUrlAvailable])

  const requestPass=()=>{
    if(name && mobile && relation && camImage){
      passTemplate.idGuardPost = 22
      passTemplate.idResident = data['resident.idResident']
      passTemplate.idProperty = auth.userProfile.idProperty
      passTemplate.idPropertyUnit = data['unit.idPropertyUnit']
      passTemplate.visitorType = visitorType
      passTemplate.currentStatus = 'AWAITING_RESPONSE_ENTRY'
      passTemplate.visitor.firstName = name
      passTemplate.visitor.profileImageUrl = camImage
      passTemplate.visitor.phoneNumber = mobile

      console.log(passTemplate)

      dispatch(CreatePassActions.postNewPassRequest(passTemplate))
      
    } else{
      Alert.alert('Warning', 'Please fill up all the fields to proceed')
    }
  }
  
  useEffect(()=>{
    if(createPass.passAdditionDone){
      dispatch(CreatePassActions.stateReset({
        passAdditionDone: false
      }))
      props.navigation.navigate('CompletedPage')
    }
  },[createPass.passAdditionDone])

  useEffect(()=>{
    if(createPass.fetching){
      setLoading(true)
    } else{
      setLoading(false)
    }
  },[createPass.fetching])

  return (
    <View style={styles.container}>
      {
        loading?
          <LottieLoading visible = {loading}/>
      : null
      }
      <DateDisplay />
      <Image style={styles.logo} source={Images.logo}/>
      <View style={styles.body}>
        <View style={styles.bodyLeft}>
          <ImageBackground source={{uri: data['resident.profileImageUrl'] ? data['resident.profileImageUrl'] : userImageUG}} style={styles.imgBg}>
            <View style={{position: 'absolute', left: 0, bottom: 0, padding: 10, width: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.50)'}}>
                <Text numberOfLines= {1} style={{color: 'white'}}>{data['resident.firstName']}</Text>
                <Text style={{color: 'white'}}>{data['unit.name']}</Text>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.bodyRight}>
          <Text style={styles.mainHeading}>Enter your Name and Mobile number to Request Pass. You will receive the pass on your mobile number</Text>
          <View style={styles.textBody}>
            <View style={{width: '75%', flexDirection: 'column', justifyContent: 'flex-start', alignItems: 'flex-start'}}>
              <TextInput style={styles.textInp1} placeholder="Enter Your Name" onChangeText={text => setName(text)} value={name}/>
              <TextInput style={styles.textInp2} placeholder="Enter Your Mobile" onChangeText={text => setMobile(text)} value={mobile}/>
            </View>
            <Pressable style={{width: '25%', height: 90, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}} onPress={()=> openCamera()}>
              {
                camImage?
                <Fragment>
                  <Image source={{uri: camImage}} style={{height: 90, width: '100%', resizeMode: 'contain'}}/>
                  <Image source={Images.cam} style={{position: 'absolute', height: '20%', resizeMode: 'contain', bottom: 10}}/>
                </Fragment>
                :
                  <Image source={Images.cam} style={styles.cam}/>
              }
            </Pressable>
          </View>
          <ButtonMain styles={styles.searchBtn} linearGradientLeft={Colors.greenGdtLeft} linearGradientRight={Colors.greenGdtRight} name="Request Pass"  onPress={()=> requestPass()}/>    
        </View>
      </View>
    </View>
  )
}
{/*<TextInput style={styles.textInp1} onChangeText={text => setName(text)} value={name}/>
            <Pressable style={styles.cameraBtn} onPress={()=>openCamera()}>
                <Image source={Images.cam} style={styles.cam}/>
                {
                  camImage?
                    <Image source={{uri: camImage}} style={{height: "100%", resizeMode: 'contain'}}/>
                  :
                    <Image source={Images.cam} style={styles.cam}/>
                }
            </Pressable>
            </View>          
            <TextInput style={styles.textInp2} onChangeText={text => setMobile(text)} value={mobile}/>
            <ButtonMain styles={styles.searchBtn} linearGradientLeft={Colors.greenGdtLeft} linearGradientRight={Colors.greenGdtRight} name="Request Pass"  onPress={()=> {}}/>
            */}  