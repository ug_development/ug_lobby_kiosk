import React, { useState } from 'react'
import { Pressable, Text, Image, View, Modal } from 'react-native'
import { Images } from '../Themes'

import DateDisplay from '../Components/DateDisplay'

// Styles
import styles from './Styles/MainPage.styles'

export default function MainPageGuest(props){

  const [guest,setGuest] = useState(false);

  return (
    <View style={styles.container}>
      <DateDisplay />
        <View style={styles.bgContainer}>
          <View style={styles.body}>
            <Pressable style={styles.bodyC} onPress={()=> props.navigation.navigate('SearchPage', {visitorType: 'GUEST', relation: 'RELATIVE'})}>
              <Image style={styles.item} source={Images.relative}/>
            </Pressable>
            <Pressable style={styles.bodyC} onPress={()=> props.navigation.navigate('SearchPage', {visitorType: 'GUEST', relation: 'FRIEND'})}>
              <Image style={styles.item} source={Images.friend}/>
            </Pressable>
            <Pressable style={styles.bodyC} onPress={()=> props.navigation.navigate('SearchPage', {visitorType: 'GUEST', relation: 'SERVICE'})}>
              <Image style={styles.item} source={Images.service}/>
            </Pressable>
          </View>
        </View>
    </View>
  )
}
