import React, { useState, useEffect } from 'react'
import { ImageBackground, Text, Image, View, TextInput, Pressable, Alert } from 'react-native'
import { Images, Colors } from '../Themes'
import {useDispatch, useSelector} from 'react-redux'
import AuthActions from '../Redux/AuthRedux'

import DateDisplay from '../Components/DateDisplay'

// Styles
import styles from './Styles/LaunchScreenStyles'

export default function Login(props){
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const dispatch = useDispatch()
  const auth = useSelector(state => state.auth)

  useEffect(() => {
    dispatch(AuthActions.stateReset({
      loggingIn: false,
      accessToken: null,
      fetching: false,

      fetchingProfile: false,
      fetchProfileSuccessful: false,
      userProfile: null,

      error: null
    }))
  }, [])

  const buttonHandler=()=>{
    if(username && password){
      dispatch(AuthActions.loginRequest(username, password))
    } else{
      Alert.alert("Enter both username and password to proceed.")
    }
  }

  //----------UE for checking error------------
  useEffect(() => {
    if (auth.error) {
      Alert.alert('Login Error', auth.error.message)
      //alert(auth.error.message)
      dispatch(AuthActions.clearError())
    }
  }, [auth.error])

  //----------UE for validating login------------
  useEffect(() => {
    if (auth.userProfile && auth.accessToken) {
      props.navigation.navigate('LaunchScreen')
    }
  }, [auth.userProfile])

  return (
    <View style={styles.container2}>
      <ImageBackground style={{width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center'}} resizeMode="cover" source={require('../Images/bg.png')}>
        <Image source={Images.darkLogo} style={{width: 60, marginLeft: 20, resizeMode: 'contain'}}/>
        <TextInput onChangeText={(text)=> setUsername(text)} placeholder='Enter Email' style={{width: '40%', borderRadius: 10, paddingLeft: 20, color: Colors.background, backgroundColor: Colors.snow, marginTop: 10}} placeholderTextColor={Colors.border}/>
        <TextInput onChangeText={(text)=> setPassword(text)} placeholder='Enter Passcode' style={{width: '40%', borderRadius: 10, paddingLeft: 20, color: Colors.background, backgroundColor: Colors.snow, marginTop: 10}} placeholderTextColor={Colors.border}/>
        <Pressable onPress={()=> buttonHandler()} style={{width: '20%', marginLeft: '20%', paddingVertical: 10, borderRadius: 5, backgroundColor: Colors.greenGdtRight, justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
          <Text style={{color: Colors.snow, fontSize: 16}}>UNLOCK</Text>
        </Pressable>
      </ImageBackground>
    </View>
  )
}