import React, { useState } from 'react'
import { Pressable, Text, Image, View, Modal } from 'react-native'
import { Images, Colors } from '../Themes'

import DateDisplay from '../Components/DateDisplay'

// Styles
import styles from './Styles/MainPage.styles'

export default function MainPage(props){

  return (
    <View style={styles.container}>
      <DateDisplay />
      <Image style={styles.logo} source={Images.logo}/>
      <View style={styles.body}>
        <Pressable style={styles.bodyC} onPress={()=> props.navigation.navigate('MainPageGuest', {visitorType: 'GUEST'})}>
          <Image style={styles.item} source={Images.guest}/>
        </Pressable>
        <Pressable style={styles.bodyC} onPress={()=> props.navigation.navigate('SearchPage', {visitorType: 'MATERIAL', relation: 'NONE'})}>
          <Image style={styles.item} source={Images.material}/>
        </Pressable>
        <Pressable style={styles.bodyC} onPress={()=> props.navigation.navigate('SearchPage', {visitorType: 'DELIVERY', relation: 'NONE'})}>
          <Image style={styles.item} source={Images.delivery}/>
        </Pressable>
      </View>
      <Pressable onPress={()=> props.navigation.navigate('PasscodePage')} style={{paddingVertical: 10, paddingHorizontal: 10, position: 'absolute', bottom: 50, backgroundColor: Colors.background}}>
        <Text style={{fontSize: 18, color: Colors.greenGdtLeft}}>Got the Passcode?</Text>
      </Pressable>
    </View>
  )
}
