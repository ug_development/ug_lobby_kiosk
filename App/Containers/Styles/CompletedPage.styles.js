import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../Themes'
const { height, width } = Dimensions.get('window')

export default StyleSheet.create({

  container: {
    width: width*1.1,
    height: height,
    backgroundColor: Colors.background,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    position: 'absolute',
    top: height*0.1,
    left: width*0.05,
    height: height*0.12,
    width: height*0.12,
    resizeMode: 'contain',
  },
  tick: {
    height: height*0.30, 
    width: height*0.30,
    resizeMode: 'contain'
  },
  body: {    
    height: height*0.50, 
    width: width,
    flexDirection: "column",
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  searchBtn: {
    width: '90%',
    height: '20%',
    margin: 10,    
    marginTop: height*0.05,
  },
  mainHeading: {
    color: Colors.snow,
    marginHorizontal: 10,
    lineHeight: 25,
    fontSize: height*0.045,
    marginVertical: height*0.04
  },
  subHeading: {
    color: Colors.ricePaper,
    fontSize: height*0.045,
  }

  
})
