import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../Themes/'
const { height, width } = Dimensions.get('window')

export default StyleSheet.create({

  container: {
    width: width*1.1,
    height: height,
    backgroundColor: Colors.background,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  container2: {
    width: width*1.1,
    height: height,
    backgroundColor: Colors.background,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    position: 'absolute',
    top: height*0.1,
    left: width*0.05,
    height: height*0.12,
    width: height*0.12,
    resizeMode: 'contain',
  },
  mainHeading: {
    color: Colors.snow,
    fontSize: height*0.065,
    marginVertical: height*0.05
  },
  subHeading: {
    color: Colors.ricePaper,
    fontSize: height*0.045,
  }

  
})
