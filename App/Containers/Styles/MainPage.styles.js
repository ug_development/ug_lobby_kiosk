import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../Themes'
const { height, width } = Dimensions.get('window')

export default StyleSheet.create({

  container: {
    width: width*1.1,
    height: height,
    backgroundColor: Colors.background,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  bgContainer: {
    backgroundColor: Colors.background,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    position: 'absolute',
    top: height*0.1,
    left: width*0.05,
    height: height*0.12,
    width: height*0.12,
    resizeMode: 'contain',
  },
  header: {
    marginTop: 30,
    height: '25%',
    width: '100%',
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textInp: {
    width: '100%',
    height: '100%',
    borderRadius: height*0.02,
    backgroundColor: Colors.snow,
  },
  textInp2: {
    width: '100%',
    height: '100%',
    borderRadius: height*0.02,
    backgroundColor: Colors.snow,
    fontSize: 20,
    letterSpacing: 24
  },
  searchBtn: {
    width: '20%',
    borderRadius: 10,
    height: '100%',
  },
  searchBtn2: {
    width: '30%',
    borderRadius: 10,
    height: '100%'
  },
  searchBtn3: {
    width: '40%',
    borderRadius: 10,
    height: 45
  },
  item: {
    width: '90%',
    height: '90%',
    resizeMode: 'contain',
  },
  body: {
    marginTop: height*0.10,
    height: height*0.35,
    width: width*0.80,
    flexDirection: "row",
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  bodyC: {
    height: '100%',
    width: "33%",
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  mainHeading: {
    color: Colors.snow,
    fontSize: height*0.065,
    marginVertical: height*0.05
  },
  subHeading: {
    color: Colors.ricePaper,
    fontSize: height*0.045,
  }

  
})
