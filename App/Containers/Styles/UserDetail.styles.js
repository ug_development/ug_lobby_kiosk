import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../Themes'
const { height, width } = Dimensions.get('window')

export default StyleSheet.create({

  container: {
    width: width*1.1,
    height: height,
    backgroundColor: Colors.backgroundLight,
    flexDirection: "column",
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    position: 'absolute',
    top: height*0.1,
    left: width*0.05,
    height: height*0.12,
    width: height*0.12,
    resizeMode: 'contain',
  },
  header: {
    marginTop: height*0.30,
    height: '12%',
    width: '90%',
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textBody: {
    width: '95%',
    //height: '80%',
    marginVertical: 10,
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textInp1: {
    width: '90%',
    paddingLeft: 20,
    height: 40,
    marginLeft: 10,
    marginBottom: height*0.03,
    borderRadius: height*0.01,
    backgroundColor: Colors.snow,
  },  
  textInp2: {
    width: '90%',
    height: 40,
    paddingLeft: 20,
    marginLeft: 10,
    borderRadius: height*0.01,
    backgroundColor: Colors.snow,
  },  
  cameraBtn: {
    width: '20%',
    height: '100%',
  },
  cam: {
    height: '70%', 
    resizeMode: 'contain'
  },
  searchBtn: {
    width: '90%',
    height: '20%',
    margin: 10,    
    marginTop: height*0.05,
  },
  imgBg: {
    width: '100%', 
    height: '100%', 
    position: 'relative',
    borderRadius: 15,
  },
  body: {
    width: '80%',
    height: '70%', 
    marginTop: '10%',
    backgroundColor: Colors.background,
    flexDirection: "row",
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    borderRadius: 15,
    overflow: 'hidden'
  },
  bodyLeft: {
    width: '30%',
    height: '100%', 
    backgroundColor: 'blue',
  },
  bodyRight: {
    width: '70%',
    height: '100%', 
    paddingVertical: 20,
    paddingHorizontal: 10,
    //backgroundColor: 'green',
  },
  mainHeading: {
    color: Colors.snow,
    marginHorizontal: 10,
    lineHeight: 25,
    fontSize: height*0.035,
    //marginVertical: height*0.05
  },
  subHeading: {
    color: Colors.ricePaper,
    fontSize: height*0.045,
  }

  
})
