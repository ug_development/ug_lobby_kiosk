import { StyleSheet, Dimensions } from 'react-native'
import { Colors } from '../../Themes'
const { height, width } = Dimensions.get('window')

export default StyleSheet.create({

  container: {
    width: width*1.1,
    height: height,
    backgroundColor: Colors.background,
    flexDirection: "column",
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingRight: 20,
  },
  logo: {
    position: 'absolute',
    top: height*0.1,
    left: width*0.05,
    height: height*0.12,
    width: height*0.12,
    resizeMode: 'contain',
  },
  header: {
    marginTop: height*0.30,
    height: '12%',
    width: '90%',
    flexDirection: "row",
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textInp: {
    width: '75%',
    height: '100%',
    borderRadius: height*0.02,
    backgroundColor: Colors.snow,
  },
  searchBtn: {
    width: '20%',
    borderRadius: 10,
    height: '100%',
  },
  body: {
    marginTop: '3%',
    height: '40%',
    //backgroundColor: 'red',
    width: '90%',
  },
  mainHeading: {
    color: Colors.snow,
    fontSize: height*0.065,
    marginVertical: height*0.05
  },
  subHeading: {
    color: Colors.ricePaper,
    fontSize: height*0.045,
  }

  
})
