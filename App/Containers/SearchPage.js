import React, { useState, useEffect } from 'react'
import { Pressable, Text, Image, View, ScrollView, TextInput } from 'react-native'
import { Images, Colors } from '../Themes'

import CreatePassActions from '../Redux/CreatePassRedux'
import DateDisplay from '../Components/DateDisplay'
import { useDispatch, useSelector } from 'react-redux'

// Styles
import styles from './Styles/SearchPage.styles'
import ButtonMain from '../Components/ButtonMain'
import UserCard from '../Components/UserCard'

const searchData =[
  {id: '1', name: 'Aju Abdul', unit: '12 B', imgUrl: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-3-300x300.png'},
  {id: '2', name: 'Samad Abdul', unit: '10 B', imgUrl: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-2.png'},
  {id: '3', name: 'Arnab', unit: '11 B', imgUrl: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-1.png'},
  {id: '4', name: 'Aju Abdul', unit: '15 B', imgUrl: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-3-300x300.png'},
  {id: '5', name: 'Aju Abdul', unit: '17 B', imgUrl: 'https://widgetwhats.com/app/uploads/2019/11/free-profile-photo-whatsapp-3-300x300.png'}
];

export default function SearchPage(props){
  const relation = props.navigation.state.params && props.navigation.state.params.relation ? props.navigation.state.params.relation : ''
  const visitorType = props.navigation.state.params && props.navigation.state.params.visitorType ? props.navigation.state.params.visitorType : ''
  const createPass = useSelector(store => store.createPass);
  const auth = useSelector(store => store.auth);
  const dispatch = useDispatch();

  const [value, setValue] = useState('');
  const [residents, setResidents] = useState([]);
  console.log(residents)

  useEffect(()=>{
    if(createPass.allResidentsList){
      setResidents(createPass.allResidentsList)
      dispatch(CreatePassActions.stateReset({
        receivedResidents: false,
        allResidentsList: null
      }))
    }
  },[createPass.allResidentsList])

  const searchForResident=()=>{
    dispatch(CreatePassActions.getAllResidentsRequest(value))
  }

  return ( 
    <View style={styles.container}>
      <DateDisplay />
      <Image style={styles.logo} source={Images.logo}/>
      <View style={styles.header}>
        <TextInput style={styles.textInp} onChangeText={text => setValue(text)} value={value}/>
        <ButtonMain styles={styles.searchBtn} linearGradientLeft={Colors.greenGdtLeft} linearGradientRight={Colors.greenGdtRight} name="Search"  onPress={()=> searchForResident()}/>
      </View>
      {
        residents ?(
          <Text style={{alignSelf: 'flex-start', marginLeft: '5%', color: 'white', marginVertical: '1%'}}>We found {residents.length} result for "{value}"</Text>
        ) : (
          null
        )                                                                                                                                                                                       
      }
      <View style={styles.body}>
        <ScrollView horizontal={true} contentContainerStyle={{height: '100%'}}>
          {residents && residents.map((user, index)=> <UserCard key={index} data={user} visitorType={visitorType} relation={relation} {...props}/>)} 
        </ScrollView>
      </View>
    </View>
  )
}
