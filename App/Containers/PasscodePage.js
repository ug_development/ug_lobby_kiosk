import React, { useState, useEffect } from 'react'
import { Alert, Text, Image, View, TextInput, Modal } from 'react-native'
import { Images, Colors } from '../Themes'
import {useDispatch, useSelector} from 'react-redux'
import CreatePassActions from '../Redux/CreatePassRedux'
import LottieLoading from '../Components/LottieLoading'

import DateDisplay from '../Components/DateDisplay'
import ButtonMain from '../Components/ButtonMain'

// Styles
import styles from './Styles/MainPage.styles'

export default function PasscodePage(props){
  const [passCodeText, setPassCodeText] = useState('');
  const [show, setShow] = useState(false);
  const [pass, setPass] = useState('');
  const dispatch = useDispatch()
  const [loading, setLoading] = useState(false)
  const createPass = useSelector(state => state.createPass)

  const buttonHandler=()=>{
    if(passCodeText){
      dispatch(CreatePassActions.getPassRequest(passCodeText))
    } else{
      Alert.alert("Warning", "Enter the passcode received to your mobile phone from the host to proceed.")
    }
  }

  useEffect(()=>{
    dispatch(CreatePassActions.stateReset({
      passError: null,
      error: null,
      fetching: false
    }))
  },[])

  useEffect(()=>{
    if(createPass.passError){
      Alert.alert("Invalid Passcode", "The entered passcode is invalid. Please try again")
      dispatch(CreatePassActions.stateReset({
        passError: null,
        error: null
      }))
    }
  },[createPass.passError])

  useEffect(()=>{
    if(createPass.livePass){
      setPass(createPass.livePass)
      const scanPass = {
        idVisitorPasses: [createPass.livePass.idVisitorPass],
        scanType: "ENTRY",
        idGuardPost: 22,
        action: "APPROVE",
        deniedReason: ""
      }
      dispatch(CreatePassActions.rescanRequest(scanPass))
      setShow(true)
      dispatch(CreatePassActions.stateReset({
        livePass: null,
        error: null
      }))
    }
  },[createPass.livePass])

  useEffect(()=>{
    if(createPass.fetching){
      setLoading(true)
    } else{
      setLoading(false)
    }
  },[createPass.fetching])

  return (
    <View style={styles.container}>
      {
        loading?
          <LottieLoading visible = {loading}/>
      : null
      }
      {
        show ? (
          <Modal visible={show} transparent>
            <View style={{width: '100%', height: '100%', justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.modalBGTop}}>
              <View style={{width: '40%', backgroundColor: Colors.snow, paddingVertical: 20, alignItems: 'center', borderRadius: 10, justifyContent: 'space-between'}}>
                <View style={{width: '100%', justifyContent: 'center', alignItems: 'center', marginBottom: 30}}>                  
                  <Text style={{color: Colors.background, marginVertical: 15, fontSize: 20}}>ENTRY ALLOWED</Text>
                  <Image source={{uri: pass.visitor.profileImageUrl}} style={{width: 75, height: 75, resizeMode: 'cover', borderRadius: 100}}/>
                  <Text style={{color: Colors.modalBGTop, marginTop: 10, fontSize: 16}}>{pass.visitor.firstName}</Text>
                  <Text style={{color: Colors.modalBGTop, marginTop: 5, fontSize: 16}}>{pass.visitor.phoneNumber}</Text>
                </View>
                <ButtonMain styles={styles.searchBtn3} linearGradientLeft={Colors.greenGdtLeft} linearGradientRight={Colors.greenGdtLeft} name="OK"  onPress={()=> {return(setShow(false), props.navigation.navigate('LaunchScreen'))}}/>
              </View>
            </View>
          </Modal>
        ) : (
          null
        )
      }
      <DateDisplay />
      <Image style={styles.logo} source={Images.logo}/>
      <View style={{width: '40%', height: '50%', marginTop: 50}}>
        <View style={styles.header}>
          <TextInput style={styles.textInp2} onChangeText={text => setPassCodeText(text)} value={passCodeText}/>
        </View>
        <View style={styles.header}>
          <ButtonMain styles={styles.searchBtn2} linearGradientLeft={Colors.redGdtLeft} linearGradientRight={Colors.redGdtRight} name="Back"  onPress={()=> props.navigation.goBack()}/>
          <ButtonMain styles={styles.searchBtn2} linearGradientLeft={Colors.greenGdtLeft} linearGradientRight={Colors.greenGdtRight} name="Search"  onPress={()=> buttonHandler()}/>
        </View>
      </View>
    </View>
  )
}
