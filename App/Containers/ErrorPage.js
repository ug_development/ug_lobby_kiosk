import React, { useState } from 'react'
import { Pressable, Text, Image, View, Modal, TextInput } from 'react-native'
import { Images, Colors } from '../Themes'

import DateDisplay from '../Components/DateDisplay'

// Styles
import styles from './Styles/ErrorPage.styles'
import ButtonMain from '../Components/ButtonMain'

export default function ErrorPage(props){

  return (
    <View style={styles.container}>
      <DateDisplay />
      <Image style={styles.logo} source={Images.logo}/>
      <Pressable style={styles.body} onPress={()=> props.navigation.navigate('CompletedPage')}>
        <Image source={Images.err} style={styles.err}/>
        <Text style={styles.subHeading}>OOPS!</Text>
        <Text style={styles.mainHeading}>Looks like something went wrong</Text>
      </Pressable>
      <ButtonMain styles={styles.searchBtn} linearGradientLeft={Colors.greenGdtLeft} linearGradientRight={Colors.greenGdtRight} name="Reset"  onPress={()=> props.navigation.navigate('LaunchScreen')}/>
    </View>
  )
}
