import React, { Component } from 'react'
import { Pressable, Text, Image, View } from 'react-native'
import { Images } from '../Themes'

import DateDisplay from '../Components/DateDisplay'

// Styles
import styles from './Styles/LaunchScreenStyles'

export default function LaunchScreen(props){

  return (
    <Pressable style={styles.container} onPress={()=> props.navigation.navigate('MainPage')}>
      <DateDisplay />
      <Image style={styles.logo} source={Images.logo}/>
      <Text style={styles.subHeading}>Good Evening</Text>
      <Text style={styles.mainHeading}>Touch the screen to start</Text>
    </Pressable>
  )
}