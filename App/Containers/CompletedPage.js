import React, { useState, useEffect } from 'react'
import { Pressable, Text, Image, View, Modal, TextInput } from 'react-native'
import { Images } from '../Themes'

import DateDisplay from '../Components/DateDisplay'

// Styles
import styles from './Styles/CompletedPage.styles'
import ButtonMain from '../Components/ButtonMain'

export default function CompletedPage(props){

  const [value, setValue] = useState('');
  
  useEffect(() => {
    const timer = setTimeout(() => {
      props.navigation.navigate('LaunchScreen');
    }, 6000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <View style={styles.container}>
      <DateDisplay />
      <Image style={styles.logo} source={Images.logo}/>
      <Pressable style={styles.body} onPress={()=> props.navigation.navigate('ErrorPage')}>
        <Image source={Images.tick} style={styles.tick}/>
        <Text style={styles.mainHeading}>You will receive your pass from your host shortly.</Text>
        <Text style={styles.subHeading}>Have a nice day.</Text>
      </Pressable>
      
    </View>
  )
}
