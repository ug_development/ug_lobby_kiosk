import { none } from 'ramda';
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack';
import Login from '../Containers/Login'
import LaunchScreen from '../Containers/LaunchScreen'
import MainPage from '../Containers/MainPage'
import MainPageGuest from '../Containers/MainPageGuest'
import SearchPage from '../Containers/SearchPage'
import ErrorPage from '../Containers/ErrorPage'
import UserDetail from '../Containers/UserDetail'
import CompletedPage from '../Containers/CompletedPage'
import PasscodePage from '../Containers/PasscodePage'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = createStackNavigator({
  Login: { screen: Login },
  LaunchScreen: { screen: LaunchScreen },
  MainPageGuest: {screen: MainPageGuest},
  MainPage: {screen: MainPage},
  SearchPage: {screen: SearchPage},
  ErrorPage: {screen: ErrorPage},
  UserDetail: {screen: UserDetail},
  CompletedPage: {screen: CompletedPage},
  PasscodePage: {screen: PasscodePage},
  
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'Login',
  navigationOptions: {
    headerStyle: none
  }
})

export default createAppContainer(PrimaryNav)
