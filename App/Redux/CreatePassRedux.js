import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  stateReset: ['resetState'], 
  clearError: null,

  getAllResidentsRequest: ['value'],
  getAllResidentsSuccess: ['allResidentsList'],
  getAllResidentsFailure: ['error'],

  visitorPhotoUploadRequest: ['formdata'],
  visitorPhotoUploadSuccess: ['photoUrl'],
  visitorPhotoUploadFailure: ['error'],

  postNewPassRequest: ['passTemplate'],
  postNewPassSuccess: null,
  postNewPassFailure: ['error'],

  getPassRequest: ['passCodeText'],
  getPassSuccess: ['passType', 'livePass', 'associatedPasses'],
  getPassFailure: ['error'],

  rescanRequest: ['scanPass'],
  rescanSuccess: ['latestPassUpdate'],
  rescanFailure: ['error'],
})

export const CreatePassTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  fetching: false,
  
  visitorsList: null,
  allResidentsList: null,
  receivedResidents: false,
  visitorPhoto: null,
  passType: null,
  livePass: null,
  passReceived: false, 
  associatedPasses: null,
  photoUrlAvailable: false,
  passUpdated: false,
  latestPassUpdate: false,

  passAdditionDone : false,
  
  error: null
})

/* ------------- Selectors ------------- */

export const CreatePassSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */

export const clearError = (state) => state.merge({ error: null })
export const stateReset = (state, { resetState }) => state.merge(resetState)

export const getAllResidentsRequest = (state,) =>  state.merge({ fetching: true })
export const getAllResidentsSuccess = (state, { allResidentsList }) =>  state.merge({ fetching: false, error: null, allResidentsList, receivedResidents: true })
export const getAllResidentsFailure = (state, { error }) =>  state.merge({ fetching: false, error })

export const visitorPhotoUploadRequest = (state) =>  state.merge({ categoryList: null, doneAlert: false })
export const visitorPhotoUploadSuccess = (state, {photoUrl}) =>   state.merge({  error: null, visitorPhoto: photoUrl, photoUrlAvailable : true })
export const visitorPhotoUploadFailure = (state, {error}) =>   state.merge({  error })

export const postNewPassRequest = (state) =>  state.merge({ fetching: true })
export const postNewPassSuccess = (state) =>   state.merge({ fetching: false, error: null, passAdditionDone: true })
export const postNewPassFailure = (state, { error }) =>  state.merge({ fetching: false, error })

export const getPassRequest = (state) =>  state.merge({ fetching: true })
export const getPassSuccess = (state, { passType, livePass, associatedPasses }) =>  state.merge({ fetching: false, error: null, passType: passType, livePass: livePass, passReceived: true, associatedPasses })
export const getPassFailure = (state, { error }) =>  state.merge({ fetching: false, passError: error })

export const rescanRequest = (state) =>  state.merge({  })
export const rescanSuccess = (state, { latestPassUpdate }) =>   state.merge({ error: null, passUpdated: true, latestPassUpdate })
export const rescanFailure = (state, { error }) =>  state.merge({ error })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CLEAR_ERROR]: clearError,
  [Types.STATE_RESET]: stateReset,
  
  [Types.GET_ALL_RESIDENTS_REQUEST]: getAllResidentsRequest,
  [Types.GET_ALL_RESIDENTS_SUCCESS]: getAllResidentsSuccess,
  [Types.GET_ALL_RESIDENTS_FAILURE]: getAllResidentsFailure,

  [Types.VISITOR_PHOTO_UPLOAD_REQUEST]: visitorPhotoUploadRequest,
  [Types.VISITOR_PHOTO_UPLOAD_SUCCESS]: visitorPhotoUploadSuccess,
  [Types.VISITOR_PHOTO_UPLOAD_FAILURE]: visitorPhotoUploadFailure,

  [Types.POST_NEW_PASS_REQUEST]: postNewPassRequest,
  [Types.POST_NEW_PASS_SUCCESS]: postNewPassSuccess,
  [Types.POST_NEW_PASS_FAILURE]: postNewPassFailure,  
  
  [Types.GET_PASS_REQUEST]: getPassRequest,
  [Types.GET_PASS_SUCCESS]: getPassSuccess,
  [Types.GET_PASS_FAILURE]: getPassFailure,

  [Types.RESCAN_REQUEST]: rescanRequest,
  [Types.RESCAN_SUCCESS]: rescanSuccess,
  [Types.RESCAN_FAILURE]: rescanFailure,
})
