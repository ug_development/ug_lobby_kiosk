import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({

  stateReset: ['resetState'],
  clearError: null,

  loginRequest: ['username', 'password'],
  loginFailure: ['error'],
  loginSuccess: ['userProfile', 'accessToken'],

  getProfileRequest: null,
  getProfileFailure: ['error'],
  getProfileSuccess: ['userProfle'],

  logout: null 
})

export const AuthTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
   fetching: false,
  loggingIn: false,
  accessToken: null,
  scheduleLogout: false,

  fetchingProfile: false,
  fetchProfileSuccessful: false,
  userProfile: null,
  updatedImageUrl: false,

  error: null
})

/* ------------- Selectors ------------- */

export const AuthSelectors = {
  getData: state => state.data
}

/* ------------- Reducers ------------- */
export const clearError = (state) => state.merge({ error: null, scheduleLogout: false, fetching: false })
export const stateReset = (state, { resetState }) => state.merge(resetState)

export const loginRequest = (state) => state.merge({ loggingIn: true, fetching: true, scheduleLogout: false })
export const loginFailure = (state, {error}) => state.merge({ loggingIn: false, error, fetching: false, scheduleLogout: false })
export const loginSuccess = (state, {userProfile, accessToken}) => state.merge({ loggingIn: false, userProfile, accessToken, fetching: false, scheduleLogout: false})

export const getProfileRequest = (state) => state.merge({ fetchingProfile: true, fetchProfileSuccessful: false, fetching: true, scheduleLogout: false })
export const getProfileFailure = (state, {error}) => state.merge({ fetchingProfile: false, fetchProfileSuccessful: false, error, fetching: false, scheduleLogout: false })
export const getProfileSuccess = (state, {userProfile}) => state.merge({ fetchingProfile: false, fetchProfileSuccessful: true, userProfile, fetching: false, scheduleLogout: false })

export const logout = state => state.merge({ ...INITIAL_STATE })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.CLEAR_ERROR]: clearError,
  [Types.STATE_RESET]: stateReset,

  [Types.LOGIN_REQUEST]: loginRequest,
  [Types.LOGIN_FAILURE]: loginFailure, 
  [Types.LOGIN_SUCCESS]: loginSuccess,

  [Types.GET_PROFILE_REQUEST]: getProfileRequest,
  [Types.GET_PROFILE_FAILURE]: getProfileFailure,
  [Types.GET_PROFILE_SUCCESS]: getProfileSuccess,

  [Types.LOGOUT]: logout
})
