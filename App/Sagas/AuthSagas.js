import { call, put } from 'redux-saga/effects'
import AuthActions from '../Redux/AuthRedux'

export function * loginPost (api, {username, password}) {
  const response = yield call(api.login, username, password)
  console.log(response)
  if (response && response.ok && response.data && response.data.id) {
    const accessToken = response.data.id
    const idGuard = response.data.userId
    const responseProfile = yield call(api.getProfile, accessToken)
    console.log(responseProfile)
    if (responseProfile && responseProfile.ok && responseProfile.data) {
      const userProfile = responseProfile.data
      yield put(AuthActions.loginSuccess(userProfile, accessToken))
      console.log(userProfile)
    } else {
      let errorMessage = responseProfile && responseProfile.data && responseProfile.data.error ? responseProfile.data.error : 'Unknown error'
      yield put(AuthActions.loginFailure(errorMessage))
    }
  } else {
    let errorMessage = response && response.data && response.data.error ? response.data.error : 'Unknown error'
    yield put(AuthActions.loginFailure(errorMessage))
  }
}

export function * fetchProfileGet (api) {
  const response = yield call(api.getProfile)
  if (response && response.ok && response.data) {
    const userProfile = response.data.data
    yield put(AuthActions.getProfileSuccess(userProfile))
  } else {
    let errorMessage = response && response.data && response.data.error ? response.data.error : 'Unknown error'
    yield put(AuthActions.getProfileFailure(errorMessage))
  }
}