import { takeLatest, all, takeEvery } from 'redux-saga/effects'
import API from '../Services/Api'

/* ------------- Types ------------- */

import { AuthTypes } from '../Redux/AuthRedux' 
import { CreatePassTypes } from '../Redux/CreatePassRedux' 

/* ------------- Sagas ------------- */
import { loginPost, fetchProfileGet } from './AuthSagas'
import { getAllResidentsRequest, visitorPhotoUploadRequest, postNewPassRequest, getPassRequest, rescanRequest } from './CreatePassSagas'

/* ------------- API ------------- */
const api = API.create() 

/* ------------- Connect Types To Sagas ------------- */

export default function * root () {
  yield all([
    takeLatest(AuthTypes.LOGIN_REQUEST, loginPost, api),
    takeLatest(AuthTypes.GET_PROFILE_REQUEST, fetchProfileGet, api),
    
    takeLatest(CreatePassTypes.GET_ALL_RESIDENTS_REQUEST, getAllResidentsRequest, api),
    takeLatest(CreatePassTypes.VISITOR_PHOTO_UPLOAD_REQUEST, visitorPhotoUploadRequest, api),
    takeLatest(CreatePassTypes.POST_NEW_PASS_REQUEST, postNewPassRequest, api),
    takeLatest(CreatePassTypes.GET_PASS_REQUEST, getPassRequest, api),
    takeLatest(CreatePassTypes.RESCAN_REQUEST, rescanRequest, api),
  ])
}
