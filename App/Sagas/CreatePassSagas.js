import { call, put } from 'redux-saga/effects'
import CreatePassActions from '../Redux/CreatePassRedux'

export function * getAllResidentsRequest (api, { value }) {
  const response = yield call(api.getAllResidents, value)
  console.log(response)
  if (response && response.ok && response.status === 200 && response.data.status === "SUCCESS") {
    const allResidentsList = response.data.data
    yield put(CreatePassActions.getAllResidentsSuccess(allResidentsList))
  } else {
    let error = response && response.data && response.data.error ? response.data.error : 'Unknown error'
    yield put(CreatePassActions.getAllResidentsFailure(error))
  }
}

export function * postNewPassRequest (api, { passTemplate }) {
  const response = yield call(api.createInstantPass, passTemplate)
  console.log(response)
  if (response && response.ok && response.status === 200) {    
    yield put(CreatePassActions.postNewPassSuccess())
  } else {
    let error = response && response.data && response.data.error ? response.data.error : 'Unknown error'
    yield put(CreatePassActions.postNewPassFailure(error))
  }
}

export function * visitorPhotoUploadRequest (api, { formdata }) {
  const response = yield call(api.fileUpload, formdata)
  console.log('++++++++++++++++++++++++++++++++++++++++++', response)
  if (response && response.ok && response.data && response.status === 200 && response.data.status === "SUCCESS") {    
    const photoUrl = response.data.url
    yield put(CreatePassActions.visitorPhotoUploadSuccess(photoUrl))
  } else {
    let errorMessage = response && response.data && response.data.error ? response.data.error : 'Unknown error'
    if (!response.data) {
      errorMessage = 'Network Error'
    }
    yield put(CreatePassActions.visitorPhotoUploadFailure(errorMessage))
  }
}


export function * getPassRequest (api, { passCodeText }) {
  const response = yield call(api.getPassRequest, passCodeText)
  console.log(response)
  if (response && response.ok && response.data.status === "SUCCESS" && response.status === 200) {
    const livePass = response.data.data.visitorPass
    const associatedPasses = response.data.data.associatedPasses ? response.data.data.associatedPasses : null
    const passType = response.data.data.visitorPass.visitorType
    yield put(CreatePassActions.getPassSuccess(passType, livePass, associatedPasses))
  } else {
    if(response.problem==="NETWORK_ERROR"){
      let error = 'Network Error'
      yield put(CreatePassActions.getPassFailure(error))
    } else{
      let error = response && response.data && response.data.status === "FAILED" && response.data.error ? response.data.error : 'Unknown error'
      yield put(CreatePassActions.getPassFailure(error))
    }
  }
}


export function * rescanRequest (api, { scanPass }) {
  const response = yield call(api.rescanRequest, scanPass)
  console.log(response)
  if (response && response.ok && response.status === 200) {
    const latestPassUpdate = {allowed : scanPass.scanType , status : scanPass.action }
    yield put(CreatePassActions.rescanSuccess(latestPassUpdate))
  } else {
    let error = response && response.data && response.data.error ? response.data.error : 'Unknown error'
    yield put(CreatePassActions.rescanFailure(error))
  }
}